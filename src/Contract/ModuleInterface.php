<?php
declare(strict_types=1);

namespace Dotgroup\NeoLMS\Contract;


interface ModuleInterface
{
    public function routes(): array;
    public function middlewares(): array;
}