<?php

namespace Dotgroup\NeoLMS\Util;

use Symfony\Component\Yaml\Yaml;
use Dotgroup\NeoLMS\Contract\ModuleInterface;

class Configurator
{
    /**
     * @var array
     */
    public $modules;

    public function __construct()
    {
        $filename = dirname(__DIR__, 5) .'/modules.yml';

        if (!is_file($filename)) {
            return;
        }

        $modules = Yaml::parse(file_get_contents($filename));

        foreach ($modules as $moduleType) {
            foreach ($moduleType as $module) {
                $module .= '\\Manifest';
                $this->addModule(new $module());
            }
        }
    }

    public function addModule(ModuleInterface $module)
    {
        if (isset($this->modules[get_class($module)])) {
            throw new \Exception('Módulo já definido!');
        }

        $this->modules[get_class($module)] = $module;
    }    
}