<?php
declare(strict_types=1);

namespace Dotgroup\NeoLMS\Contract;


interface BootstrapInterface
{
    public function run();
}